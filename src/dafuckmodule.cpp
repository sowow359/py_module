#include "Python.h"
#include "../include/htable.h"

static PyObject *DafuckError;

static PyObject *
dafuck_system(PyObject *self, PyObject *args)
{
    const char *command;
    int sts;

    if (!PyArg_ParseTuple(args, "s", &command))
        return NULL;
    sts = system(command);
    if (sts < 0) {
        PyErr_SetString(DafuckError, "System command failed");
        return NULL;
    }
    return PyLong_FromLong(sts);
}

static PyMethodDef DafuckMethods[] = {
    {"system",  dafuck_system, METH_VARARGS, "Execute a shell command."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};


static struct PyModuleDef dafuckmodule = {
    PyModuleDef_HEAD_INIT,
    "dafuck",   /* name of module */
    NULL, /* module documentation, may be NULL */
    -1,       /* size of per-interpreter state of the module,
                 or -1 if the module keeps state in global variables. */
    DafuckMethods
};

PyMODINIT_FUNC
PyInit_dafuck(void)
{
    htable<int, int> table(64);

    return PyModule_Create(&dafuckmodule);
}

