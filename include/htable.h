/*
TODO:
    iterators

Check:
    sizeof(class)
    sizeof(class<T>)
    sizeof(class<K, V>)

    sizeof in asm

    will something break if _init_size != 2^n?

*/

typedef unsigned int uint32;
typedef unsigned long long uint64;

template<class T>
uint32 my_hash(const T&key)
{
    uint32 my_hash = 0;
    uint32 size = sizeof(key);

    for (uint32 i = 0; i < size; ++i)
    {
        uint32 byte = (key >> 8 * i) & 0xff;

        my_hash += byte;
        my_hash += (my_hash << 10);
        my_hash ^= (my_hash >> 6);
    }
    my_hash += (my_hash << 3);
    my_hash ^= (my_hash >> 11);
    my_hash += (my_hash << 15);
    return my_hash;
}
template<class K, class V>
class htable_node
{
    template<class K, class V>
    friend class htable;

    htable_node<K, V>* next;
    const uint32 my_hash;

    void set(const V& new_v)
    {
        value = new_v;	// what could go wrong?
    }

public:
    const K key;
    V value;

    htable_node(const K k, V v, uint32 _hash) :
        key(k), value(v), my_hash(_hash), next(nullptr)
    {}


};

const uint32 MINIMUM_HTABLE_SIZE = 32;

template<typename K, class V>
class htable
{
    uint32 _size;
    uint32 _capacity;

    htable_node<K, V>** _items;

public:
    uint32 size()
    {
        return _size;
    }


    htable(uint32 _init_size = MINIMUM_HTABLE_SIZE)
    {
        _init(_init_size);
    }

    ~htable()
    {
        _clear();
    }

    htable_node<K, V>* operator[](const K& key)
    {
        uint32 key_hash = my_hash(key);
        uint32 index = key_hash % _capacity;

        htable_node<K, V>* node = _items[index];
        while (node)
        {
            if (node->key == key)
            {
                return node;
            }
            node = node->next;
        }
        return nullptr;
    }

    bool get(const K& key, V* value)
    {
        uint32 key_hash = my_hash(key);
        uint32 index = key_hash % _capacity;

        htable_node<K, V>* node = _items[index];
        while (node)
        {
            if (node->key == key)
            {
                *value = node->value;
                return true;
            }
            node = node->next;
        }
        return false;
    }

    bool contains(const K& key)
    {
        uint32 key_hash = my_hash(key);
        uint32 index = key_hash % _capacity;

        htable_node<K, V>* node = _items[index];
        while (node)
        {
            if (node->key == key)		// this is so wrong right?
            {
                return true;
            }
            node = node->next;
        }
        return false;
    }

    bool insert_or_replace(const K& key, const V& value)
    {
        bool result = _insert(key, value);
        if (result && _size > _capacity)
            _increase_capacity();
        return result;
    }

    bool remove(const K& key)
    {
        bool res = _remove(key);
        if (res && _size <= _capacity / 2 && _capacity > MINIMUM_HTABLE_SIZE)
            _decrease_capacity();

        return res;
    }

    void clear()
    {
        _clear();
        _init(MINIMUM_HTABLE_SIZE);
    }

private:

    void _init(uint32 _init_size)
    {
        if (_init_size < MINIMUM_HTABLE_SIZE)
            _init_size = MINIMUM_HTABLE_SIZE;
        _items = new htable_node<K, V>*[_init_size];
        _capacity = _init_size;

        // is that even necessary?
        memset(_items, 0, _init_size * sizeof(htable_node<K, V>*));
        _size = 0;
    }

    void _clear()
    {

        for (uint32 index = 0; index < _capacity; index++)
        {
            htable_node<K, V>* cur = _items[index];
            htable_node<K, V>* temp = nullptr;

            while (cur)
            {
                temp = cur->next;
                delete cur;
                _size--;
                cur = temp;
            }
        }

        delete[] _items;
    }

    void _decrease_capacity()
    {
        uint32 _new_capacity = _capacity / 2;
        //uint32 _new_mask = _new_capacity - 1;

        htable_node<K, V>** _new_items = new htable_node<K, V>*[_new_capacity];

        memset(_new_items, 0, _new_capacity * sizeof(htable_node<K, V>*));

        for (uint32 index = 0; index < _capacity; index++)
        {
            htable_node<K, V>* cur = _items[index];
            htable_node<K, V>* temp = nullptr;

            if (cur == nullptr)
                continue;

            while (cur)
            {
                uint32 new_index = cur->my_hash % _new_capacity;

                temp = cur->next;
                cur->next = _new_items[new_index];
                _new_items[new_index] = cur;
                cur = temp;
            }
        }

        _capacity = _new_capacity;
        delete[] _items;
        _items = _new_items;
    }

    bool _remove(K key)
    {
        uint32 key_hash = my_hash(key);
        uint32 index = key_hash % _capacity;

        if (_items[index] == nullptr)
        {
            return false;
        }

        htable_node<K, V>* cur = _items[index];
        htable_node<K, V>* prev = nullptr;

        while (cur)
        {
            if (cur->key == key)
            {
                if (prev)
                {
                    prev->next = cur->next;
                }
                else
                {
                    // first item in a linked list
                    _items[index] = cur->next;
                }

                delete cur;
                _size--;
                return true;
            }
            prev = cur;
            cur = cur->next;
        }
        return false;
    }


    bool _insert(K key, V value)
    {
        uint32 key_hash = my_hash(key);
        uint32 index = key_hash % _capacity;

        htable_node<K, V>* cur = _items[index];
        htable_node<K, V>* prev = nullptr;

        while (cur)
        {
            if (cur->key == key)
            {
                // replace value
                cur->set(value);
                return true;
            }
            prev = cur;
            cur = cur->next;
        }

        // key not found
        htable_node<K, V>* node = new htable_node<K, V>(key, value, key_hash);

        if (prev)
        {
            prev->next = node;
        }
        else
        {
            _items[index] = node;
        }

        _size++;
        return true;
    }

    void _increase_capacity()
    {
        uint32 _new_capacity = _capacity * 2;
        htable_node<K, V>** _new_items = new htable_node<K, V>*[_new_capacity];

        memcpy(_new_items, _items, _capacity * sizeof(htable_node<K, V>*));
        memset(&_new_items[_capacity], 0, _capacity * sizeof(htable_node<K, V>*));

        for (uint32 index = 0; index < _capacity; index++)
        {
            if (!_new_items[index])
                continue;

            htable_node<K, V>* cur = _new_items[index];
            htable_node<K, V>* temp = nullptr;
            htable_node<K, V>* prev = nullptr;

            while (cur)
            {
                uint32 new_index = cur->my_hash % _new_capacity;

                if (new_index != index)
                {
                    if (prev)
                    {
                        prev->next = cur->next;
                    }
                    else
                    {
                        _new_items[index] = cur->next;
                    }
                    temp = cur->next;
                    cur->next = _new_items[new_index];
                    _new_items[new_index] = cur;
                    cur = temp;
                }
                else
                {
                    prev = cur;
                    cur = cur->next;
                }
            }
        }

        _capacity = _new_capacity;
        delete[] _items;
        _items = _new_items;
    }
};